# Publishing

## New Virtual Env

For consistent development results, it iss best to install all dependencies into
a new virtual environment.  As there are many options out there to create a
virtual environment, the [virtualenv](https://virtualenv.pypa.io/en/latest/)
implementation seems to be the Python defacto standard for multiple reasons.
For one, it can create a virtual environment that is truly isolated.  Other
implementations create virtual environments and pip that continue to install
packages into the user or system space, and not the expected virtual
environment.  You're free to use any virtual environment as you please, if you
choose to use one at all, but xdgenvpy documentation assumes developers
are running a virtual environment like so:

```bash
python3 -m venv .venv
source .venv/bin/activate
python3 -m pip install --upgrade pip
python3 -m pip install --upgrade --requirement requirements.txt
python3 -m pip install --upgrade --requirement requirements-test.txt
python3 -m pip install --upgrade --requirement requirements-publishing.txt
```

## Publishing A New Version

```bash
python3 -m venv .venv
source .venv/bin/activate
./bin/release.sh [patch|minor|major]
```

## Pypi

Once published, the new version of [xdgenvpy](https://pypi.org/project/xdgenvpy)
should be accessibly on Pypi.
