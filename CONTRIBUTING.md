# Contributing

The best way to contribute changes to `xdgenvpy` is to fork the GitLab project,
make the changes local to your forked repository, then submit a Merge Request
through GitLab.

Although there is not formal coding standards used with `xdgenvpy`, your Merge
Request is more likely to be successful if your changes are consistent with the
existing coding style.  This includes indentations, variable/method names, and
even the amount of comments and documentation.  As far as the man-pages, for a
quick groff syntax reference, see the
[groff_man(7)](https://www.man7.org/linux/man-pages/man7/groff_man.7.html)
manpage.

Merge Requests are also more likely to be successful if the commits also include
new unit tests to ensure the new or fixed functionality actually works.  Though
there are _some_ use cases where more tests will not be necessary.  This will be
determined on a case-by-case basis.

When in doubt, create unit tests, add code comments, ensure CI jobs complete
successfully, and add as much information as you can to the Merge Request.  The
main developer will respond as soon as possible.

Finally, the commands below are intended for the main developer of `xdgenvpy`,
or anyone given maintainer/developer rights on the GitLab project.
