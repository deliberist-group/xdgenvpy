#!/usr/bin/env bash

set -o errexit
set -o errtrace
set -o noclobber
set -o nounset
set -o pipefail
# set -o xtrace

mode="${1:-empty}"
if [[ "${mode}" != "patch" && "${mode}" != "minor" && "${mode}" != "major" ]] ; then
    echo "ERROR: Unknown bumpversion mode: ${mode}" 1>&2
    exit 1
fi

root_dir="$(realpath "$(dirname "${BASH_SOURCE[0]}")/../")"
cd "${root_dir}"

source "${root_dir}/bin/setup-venv.sh"
source "${root_dir}/bin/test.sh"

python3 -m pip install  \
    --upgrade           \
    --requirement "${root_dir}/requirements-publishing.txt"

bumpversion "${mode}"
git push origin main
git push --tags

# The username with API tokens must be hard coded to this value.
export PYPI_USERNAME='__token__'

# Get the PYPI token.
token_file="${VIRTUAL_ENV:-.}/pypi.token"
if [[ ! -f "${token_file}" ]] ; then
    export PYPI_PASSWORD="$(cat "${token_file}")"
else
    # Create new API token at:  https://pypi.org/manage/account/token/
    read -r -s -p "Enter your PyPi API project token: " PYPI_PASSWORD
    export PYPI_PASSWORD
    echo "${PYPI_PASSWORD}" > "${token_file}"
fi

python3 setup.py publish
unset PYPI_PASSWORD
