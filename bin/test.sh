#!/usr/bin/env bash

set -o errexit
set -o errtrace
set -o noclobber
set -o nounset
set -o pipefail
# set -o xtrace

root_dir="$(realpath "$(dirname "${BASH_SOURCE[0]}")/../")"
cd "${root_dir}"

source "${root_dir}/bin/setup-venv.sh"

# shellcheck disable=SC2035
python3 -m black    \
    --verbose       \
    --diff          \
    --color         \
    --check         \
    xdgenvpy/       \
    xdgenvpy_test/  \
    *.py

# shellcheck disable=SC2035
python3 -m pylint   \
    --verbose       \
    xdgenvpy/       \
    xdgenvpy_test/  \
    *.py

python3 -m pytest   \
    xdgenvpy_test

type shellcheck
find .                          \
    -name '*.sh'                \
    -not -wholename '*/.venv/*' \
    -exec shellcheck {} +

yellow="$(tput setaf 3)"
green="$(tput setaf 2)"
reset="$(tput sgr0)"
echo
echo '########################################'
echo -e "${green}All tests passed.${reset}"
echo -e "${yellow}But check the logs!${reset}"
echo '########################################'
echo
