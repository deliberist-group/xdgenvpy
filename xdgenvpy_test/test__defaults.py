# pylint: disable=invalid-name
# pylint: disable=missing-module-docstring
# pylint: disable=protected-access

from ntpath import pathsep as pathsep_nt
from posixpath import pathsep as pathsep_posix

import os
import pathlib
import platform
import unittest
import unittest.mock

import xdgenvpy

_MOCKED_SYSTEM_TARGET = "xdgenvpy._defaults.platform.system"


def _freebsd():
    """
    A callable function that returns the value needed to mock the method
    :meth:`os.system()` to simulate a FreeBSD system.

    :rtype: str
    :returns: A string that simulates a FreeBSD :meth:`os.system()`.
    """
    return "FreeBSD"


@unittest.skipIf(
    "Windows" in platform.system(), "Skipping FreeBSD tests while on Windows."
)
class TestFreeBSD(unittest.TestCase):
    """Tests the default values when the system is mocked to be FreeBSD."""

    def setUp(self):
        """Determines the expected home directory."""
        self._home = pathlib.Path("~").expanduser()

    @unittest.mock.patch(_MOCKED_SYSTEM_TARGET, _freebsd)
    def test_system(self):
        """Validates that the system detected is FreeBSD."""
        self.assertEqual(_freebsd(), xdgenvpy._defaults.platform.system())

    @unittest.mock.patch(_MOCKED_SYSTEM_TARGET, _freebsd)
    def test_XDG_DATA_HOME(self):
        """Validates the default value of :code:`${XDG_DATA_HOME}`."""
        self.assertEqual(
            self._home.joinpath(".local/share").as_posix(),
            xdgenvpy._defaults.XDG_DATA_HOME(),
        )

    @unittest.mock.patch(_MOCKED_SYSTEM_TARGET, _freebsd)
    def test_XDG_CONFIG_HOME(self):
        """Validates the default value of :code:`${XDG_CONFIG_HOME}`."""
        self.assertEqual(
            self._home.joinpath(".config").as_posix(),
            xdgenvpy._defaults.XDG_CONFIG_HOME(),
        )

    @unittest.mock.patch(_MOCKED_SYSTEM_TARGET, _freebsd)
    def test_XDG_CACHE_HOME(self):
        """Validates the default value of :code:`${XDG_CACHE_HOME}`."""
        self.assertEqual(
            self._home.joinpath(".cache").as_posix(),
            xdgenvpy._defaults.XDG_CACHE_HOME(),
        )

    @unittest.mock.patch(_MOCKED_SYSTEM_TARGET, _freebsd)
    def test_XDG_RUNTIME_DIR(self):
        """Validates the default value of :code:`${XDG_RUNTIME_DIR}`."""
        self.assertEqual(
            pathlib.Path(f"/run/user/{xdgenvpy._defaults._USER_ID}").as_posix(),
            xdgenvpy._defaults.XDG_RUNTIME_DIR(),
        )

    @unittest.mock.patch(_MOCKED_SYSTEM_TARGET, _freebsd)
    def test_XDG_DATA_DIRS(self):
        """Validates the default value of :code:`${XDG_DATA_DIRS}`."""
        self.assertEqual(
            "/usr/local/share:/usr/share", xdgenvpy._defaults.XDG_DATA_DIRS()
        )

    @unittest.mock.patch(_MOCKED_SYSTEM_TARGET, _freebsd)
    def test_XDG_CONFIG_DIRS(self):
        """Validates the default value of :code:`${XDG_CONFIG_DIRS}`."""
        self.assertEqual("/etc/xdg", xdgenvpy._defaults.XDG_CONFIG_DIRS())


def _linux():
    """
    A callable function that returns the value needed to mock the method
    :meth:`os.system()` to simulate a Linux system.

    :rtype: str
    :returns: A string that simulates a Linux :meth:`os.system()`.
    """
    return "Linux"


@unittest.skipIf(
    "Windows" in platform.system(), "Skipping Linux tests while on Windows."
)
class TestLinux(unittest.TestCase):
    """Tests the default values when the system is mocked to be Linux."""

    def setUp(self):
        """Determines the expected home directory."""
        self._home = pathlib.Path("~").expanduser()

    @unittest.mock.patch(_MOCKED_SYSTEM_TARGET, _linux)
    def test_system(self):
        """Validates that the system detected is Linux."""
        self.assertEqual(_linux(), xdgenvpy._defaults.platform.system())

    @unittest.mock.patch(_MOCKED_SYSTEM_TARGET, _linux)
    def test_XDG_DATA_HOME(self):
        """Validates the default value of :code:`${XDG_DATA_HOME}`."""
        self.assertEqual(
            self._home.joinpath(".local/share").as_posix(),
            xdgenvpy._defaults.XDG_DATA_HOME(),
        )

    @unittest.mock.patch(_MOCKED_SYSTEM_TARGET, _linux)
    def test_XDG_CONFIG_HOME(self):
        """Validates the default value of :code:`${XDG_CONFIG_HOME}`."""
        self.assertEqual(
            self._home.joinpath(".config").as_posix(),
            xdgenvpy._defaults.XDG_CONFIG_HOME(),
        )

    @unittest.mock.patch(_MOCKED_SYSTEM_TARGET, _linux)
    def test_XDG_CACHE_HOME(self):
        """Validates the default value of :code:`${XDG_CACHE_HOME}`."""
        self.assertEqual(
            self._home.joinpath(".cache").as_posix(),
            xdgenvpy._defaults.XDG_CACHE_HOME(),
        )

    @unittest.mock.patch(_MOCKED_SYSTEM_TARGET, _linux)
    def test_XDG_RUNTIME_DIR(self):
        """Validates the default value of :code:`${XDG_RUNTIME_DIR}`."""
        self.assertEqual(
            pathlib.Path(f"/run/user/{xdgenvpy._defaults._USER_ID}").as_posix(),
            xdgenvpy._defaults.XDG_RUNTIME_DIR(),
        )

    @unittest.mock.patch(_MOCKED_SYSTEM_TARGET, _linux)
    def test_XDG_DATA_DIRS(self):
        """Validates the default value of :code:`${XDG_DATA_DIRS}`."""
        self.assertEqual(
            "/usr/local/share:/usr/share", xdgenvpy._defaults.XDG_DATA_DIRS()
        )

    @unittest.mock.patch(_MOCKED_SYSTEM_TARGET, _linux)
    def test_XDG_CONFIG_DIRS(self):
        """Validates the default value of :code:`${XDG_CONFIG_DIRS}`."""
        self.assertEqual("/etc/xdg", xdgenvpy._defaults.XDG_CONFIG_DIRS())


def _macos():
    """
    A callable function that returns the value needed to mock the method
    :meth:`os.system()` to simulate a Mac OS system.

    :rtype: str
    :returns: A string that simulates a Mac OS :meth:`os.system()`.
    """
    return "Darwin"


@unittest.skipIf(
    "Windows" in platform.system(), "Skipping MacOS tests while on Windows."
)
class TestMacOS(unittest.TestCase):
    """Tests the default values when the system is mocked to be Mac."""

    def setUp(self):
        """Determines the expected home directory."""
        self._home = pathlib.Path("~").expanduser()

    @unittest.mock.patch(_MOCKED_SYSTEM_TARGET, _macos)
    def test_system(self):
        """Validates that the system detected is MacOS."""
        self.assertEqual(_macos(), xdgenvpy._defaults.platform.system())

    @unittest.mock.patch(_MOCKED_SYSTEM_TARGET, _macos)
    def test_XDG_DATA_HOME(self):
        """Validates the default value of :code:`${XDG_DATA_HOME}`."""
        self.assertEqual(
            self._home.joinpath(".local/share").as_posix(),
            xdgenvpy._defaults.XDG_DATA_HOME(),
        )

    @unittest.mock.patch(_MOCKED_SYSTEM_TARGET, _macos)
    def test_XDG_CONFIG_HOME(self):
        """Validates the default value of :code:`${XDG_CONFIG_HOME}`."""
        self.assertEqual(
            self._home.joinpath(".config").as_posix(),
            xdgenvpy._defaults.XDG_CONFIG_HOME(),
        )

    @unittest.mock.patch(_MOCKED_SYSTEM_TARGET, _macos)
    def test_XDG_CACHE_HOME(self):
        """Validates the default value of :code:`${XDG_CACHE_HOME}`."""
        self.assertEqual(
            self._home.joinpath(".cache").as_posix(),
            xdgenvpy._defaults.XDG_CACHE_HOME(),
        )

    @unittest.mock.patch(_MOCKED_SYSTEM_TARGET, _macos)
    def test_XDG_RUNTIME_DIR(self):
        """Validates the default value of :code:`${XDG_RUNTIME_DIR}`."""
        self.assertEqual(
            pathlib.Path(f"/tmp/run/user/{xdgenvpy._defaults._USER_ID}").as_posix(),
            xdgenvpy._defaults.XDG_RUNTIME_DIR(),
        )

    @unittest.mock.patch(_MOCKED_SYSTEM_TARGET, _macos)
    def test_XDG_DATA_DIRS(self):
        """Validates the default value of :code:`${XDG_DATA_DIRS}`."""
        self.assertEqual(
            "/usr/local/share:/usr/share", xdgenvpy._defaults.XDG_DATA_DIRS()
        )

    @unittest.mock.patch(_MOCKED_SYSTEM_TARGET, _macos)
    def test_XDG_CONFIG_DIRS(self):
        """Validates the default value of :code:`${XDG_CONFIG_DIRS}`."""
        self.assertEqual("/etc/xdg", xdgenvpy._defaults.XDG_CONFIG_DIRS())


def _windows():
    """
    A callable function that returns the value needed to mock the method
    :meth:`os.system()` to simulate a Windows system.

    :rtype: str
    :returns: A string that simulates a Windows :meth:`os.system()`.
    """
    return "Windows"


class TestWindows(unittest.TestCase):
    """Tests the default values when the system is mocked to be Windows."""

    # A handle on the current user's name, as reported by the operating system.
    USERNAME = os.getenv("USERNAME", "xdgenvpy")

    def setUp(self):
        """Determines the expected home directory."""
        self._home = pathlib.Path(
            os.getenv("APPDATA", f"C:\\Users\\{TestWindows.USERNAME}\\AppData\\Roaming")
        )

    @unittest.mock.patch(_MOCKED_SYSTEM_TARGET, _windows)
    def test_system(self):
        """Validates that the system detected is Windows."""
        self.assertEqual(_windows(), xdgenvpy._defaults.platform.system())

    @unittest.mock.patch(_MOCKED_SYSTEM_TARGET, _windows)
    def test_XDG_DATA_HOME(self):
        """Validates the default value of :code:`${XDG_DATA_HOME}`."""
        self.assertEqual(
            self._home.joinpath("local/share").as_posix(),
            xdgenvpy._defaults.XDG_DATA_HOME(),
        )

    @unittest.mock.patch(_MOCKED_SYSTEM_TARGET, _windows)
    def test_XDG_CONFIG_HOME(self):
        """Validates the default value of :code:`${XDG_CONFIG_HOME}`."""
        self.assertEqual(
            self._home.joinpath("config").as_posix(),
            xdgenvpy._defaults.XDG_CONFIG_HOME(),
        )

    @unittest.mock.patch(_MOCKED_SYSTEM_TARGET, _windows)
    def test_XDG_CACHE_HOME(self):
        """Validates the default value of :code:`${XDG_CACHE_HOME}`."""
        self.assertEqual(
            self._home.joinpath("cache").as_posix(), xdgenvpy._defaults.XDG_CACHE_HOME()
        )

    @unittest.mock.patch(_MOCKED_SYSTEM_TARGET, _windows)
    def test_XDG_RUNTIME_DIR(self):
        """Validates the default value of :code:`${XDG_RUNTIME_DIR}`."""
        self.assertEqual(
            self._home.joinpath(f"run/user/{TestWindows.USERNAME}").as_posix(),
            xdgenvpy._defaults.XDG_RUNTIME_DIR(),
        )

    @unittest.mock.patch(_MOCKED_SYSTEM_TARGET, _windows)
    def test_XDG_DATA_DIRS(self):
        """Validates the default value of :code:`${XDG_DATA_DIRS}`."""
        self.assertEqual(
            self._home.joinpath("usr/share").as_posix(),
            xdgenvpy._defaults.XDG_DATA_DIRS(),
        )

    @unittest.mock.patch(_MOCKED_SYSTEM_TARGET, _windows)
    def test_XDG_CONFIG_DIRS(self):
        """Validates the default value of :code:`${XDG_CONFIG_DIRS}`."""
        self.assertEqual(
            self._home.joinpath("etc/xdg").as_posix(),
            xdgenvpy._defaults.XDG_CONFIG_DIRS(),
        )


def _unsupported_os():
    """
    A callable function that returns the value needed to mock the method
    :meth:`os.system()` to simulate an unsupported system.

    :rtype: str
    :returns: A string that simulates an unsupported :meth:`os.system()`.
    """
    return "DeliberistOS"


class TestUnsupportedSystem(unittest.TestCase):
    """Tests the default values when the system is unsupported."""

    @unittest.mock.patch(_MOCKED_SYSTEM_TARGET, _unsupported_os)
    def test_system(self):
        """Validates that the module can detect an unsupported OS."""
        self.assertEqual(_unsupported_os(), xdgenvpy._defaults.platform.system())

    @unittest.mock.patch(_MOCKED_SYSTEM_TARGET, _unsupported_os)
    def test_XDG_DATA_HOME(self):
        """
        Validates that the system raises an error when on an unsupported OS.
        """
        msg = "Unknown platform: " + _unsupported_os()
        with self.assertRaisesRegex(RuntimeError, msg):
            xdgenvpy._defaults.XDG_CONFIG_HOME()


class TestPathPrefixes(unittest.TestCase):
    """
    Tests the default path prefixes are configured properly based on the type
    of system.
    """

    def testFreeBSDPathPrefix(self):
        """Tests the path prefix on a FreeBSD system."""
        expected = pathlib.Path("~").expanduser()
        actual = xdgenvpy._defaults._PREFIX_PATHS[_freebsd()]
        self.assertEqual(expected, actual)

    def testLinuxPathPrefix(self):
        """Tests the path prefix on a Linux system."""
        expected = pathlib.Path("~").expanduser()
        actual = xdgenvpy._defaults._PREFIX_PATHS[_linux()]
        self.assertEqual(expected, actual)

    def testMacOSPathPrefix(self):
        """Tests the path prefix on a MacOS system."""
        expected = pathlib.Path("~").expanduser()
        actual = xdgenvpy._defaults._PREFIX_PATHS[_macos()]
        self.assertEqual(expected, actual)

    def testWindowsPathPrefix(self):
        """Tests the path prefix on a Windows system."""
        # Ensure %APPDATA% is not set.
        if "APPDATA" in os.environ:
            del os.environ["APPDATA"]
        expected = pathlib.Path(
            f"C:\\Users\\{xdgenvpy._defaults._USER_NAME}\\AppData\\Roaming"
        )
        actual = xdgenvpy._defaults._PREFIX_PATHS[_windows()]
        self.assertEqual(expected, actual)

    @unittest.mock.patch(_MOCKED_SYSTEM_TARGET, _unsupported_os)
    def testUnsupportedSystem(self):
        """
        Validates that the system raises an error when on an unsupported OS.
        """
        msg = "Unknown platform: " + _unsupported_os()
        with self.assertRaisesRegex(RuntimeError, msg):
            # The assertIsNone() won't actually get test, but it is used just to
            # silence PyCharm warnings that the the statement has not effect.
            self.assertIsNone(xdgenvpy._defaults._PREFIX_PATHS[_unsupported_os()])


class TestPathSeparators(unittest.TestCase):
    """
    Tests the default path separators are configured properly based on the type
    of system.
    """

    def testFreeBSDPathSeparator(self):
        """Tests the path separators on a FreeBSD system."""
        expected = pathsep_posix
        actual = xdgenvpy._defaults._PATH_SEPARATORS[_freebsd()]
        self.assertEqual(expected, actual)

    def testLinuxPathSeparator(self):
        """Tests the path separators on a Linux system."""
        expected = pathsep_posix
        actual = xdgenvpy._defaults._PATH_SEPARATORS[_linux()]
        self.assertEqual(expected, actual)

    def testMacOSPathSeparator(self):
        """Tests the path separators on a MacOS system."""
        expected = pathsep_posix
        actual = xdgenvpy._defaults._PATH_SEPARATORS[_macos()]
        self.assertEqual(expected, actual)

    def testWindowsPathSeparator(self):
        """Tests the path separators on a Windows system."""
        expected = pathsep_nt
        actual = xdgenvpy._defaults._PATH_SEPARATORS[_windows()]
        self.assertEqual(expected, actual)

    @unittest.mock.patch(_MOCKED_SYSTEM_TARGET, _unsupported_os)
    def testUnsupportedSystem(self):
        """
        Validates that the system raises an error when on an unsupported OS.
        """
        msg = "Unknown platform: " + _unsupported_os()
        with self.assertRaisesRegex(RuntimeError, msg):
            # The assertIsNone() won't actually get test, but it is used just to
            # silence PyCharm warnings that the the statement has not effect.
            self.assertIsNone(xdgenvpy._defaults._PATH_SEPARATORS[_unsupported_os()])
