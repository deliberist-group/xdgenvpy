# pylint: disable=invalid-name
# pylint: disable=missing-module-docstring

import os
import pathlib
import shutil
import tempfile
import unittest

import xdgenvpy.xdgenv


def _unset(*args):
    """
    Deletes a sequence of environment variables.

    :param list args: Sequence of environment variables to delete.
    """
    for var in args:
        if var in os.environ:
            del os.environ[var]


def _set(**kwargs):
    """
    Sets the sequence of environment variables.

    :param dict kwargs: A dictionary of environment variables to set.
    """
    for key, value in kwargs.items():
        os.environ[key] = str(value)


class TestXDGSetters(unittest.TestCase):
    """
    This test case ensures the setter methods on the properties raise
    exceptions as the :class:`XDG` class attempts to be as immutable as
    Pythonically possible..
    """

    def setUp(self):
        """Sets up a new object for testing setters."""
        self.xdg = xdgenvpy.xdgenv.XDG()

    def test_data_home_default(self):
        """Ensures the setter for :code:`XDG_DATA_HOME` is disabled."""
        with self.assertRaises(AttributeError):
            self.xdg.XDG_DATA_HOME = "should not work!"

    def test_config_home_default(self):
        """Ensures the setter for :code:`XDG_CONFIG_HOME` is disabled."""
        with self.assertRaises(AttributeError):
            self.xdg.XDG_CONFIG_HOME = "should not work!"

    def test_cache_home_default(self):
        """Ensures the setter for :code:`XDG_CACHE_HOME` is disabled."""
        with self.assertRaises(AttributeError):
            self.xdg.XDG_CACHE_HOME = "should not work!"

    def test_runtime_dir_default(self):
        """Ensures the setter for :code:`XDG_RUNTIME_DIR` is disabled."""
        with self.assertRaises(AttributeError):
            self.xdg.XDG_RUNTIME_DIR = "should not work!"

    def test_data_dirs_default(self):
        """Ensures the setter for :code:`XDG_DATA_DIRS` is disabled."""
        with self.assertRaises(AttributeError):
            self.xdg.XDG_DATA_DIRS = "should not work!"

    def test_config_dirs_default(self):
        """Ensures the setter for :code:`XDG_CONFIG_DIRS` is disabled."""
        with self.assertRaises(AttributeError):
            self.xdg.XDG_CONFIG_DIRS = "should not work!"


class TestXDGGetters(unittest.TestCase):
    """
    This test case covers the conditions when the XDG environment variables are
    defined and the values defined in the external shell are returned.

    Note that these tests reset the actual environment variables just to ensure
    consistency between test environments.  The point is that the production
    code retrieves values from the system environ dictionary.
    """

    def setUp(self):
        """Sets up a new object for testing getters."""
        self.xdg = xdgenvpy.xdgenv.XDG()

    def test_data_home_default(self):
        """Validates the getter for :code:`XDG_DATA_HOME`."""
        d = "/my/custom/data"
        _set(XDG_DATA_HOME=d)
        self.assertEqual(d, self.xdg.XDG_DATA_HOME)

    def test_config_home_default(self):
        """Validates the getter for :code:`XDG_CONFIG_HOME`."""
        d = "/my/custom/config"
        _set(XDG_CONFIG_HOME=d)
        self.assertEqual(d, self.xdg.XDG_CONFIG_HOME)

    def test_cache_home_default(self):
        """Validates the getter for :code:`XDG_CACHE_HOME`."""
        d = "/my/custom/cache"
        _set(XDG_CACHE_HOME=d)
        self.assertEqual(d, self.xdg.XDG_CACHE_HOME)

    def test_runtime_dir_default(self):
        """Validates the getter for :code:`XDG_RUNTIME_DIR`."""
        d = "/my/custom/runtime"
        _set(XDG_RUNTIME_DIR=d)
        self.assertEqual(d, self.xdg.XDG_RUNTIME_DIR)

    def test_data_dirs_default(self):
        """Validates the getter for :code:`XDG_DATA_HOME`."""
        ds = "::/my/custom/data/dir1:/my/custom/data/dir2::/my/custom/data/dir3::"
        ds = ds.replace(":", os.pathsep)
        _set(XDG_DATA_DIRS=ds)
        expected_ds = [self.xdg.XDG_DATA_HOME] + ds.split(":")
        expected_ds = os.pathsep.join(expected_ds)
        self.assertEqual(expected_ds, self.xdg.XDG_DATA_DIRS)

    def test_config_dirs_default(self):
        """Validates the getter for :code:`XDG_CONFIG_HOME`."""
        ds = ":/my/custom/config/dir1:::/my/custom/config/dir2::"
        ds = ds.replace(":", os.pathsep)
        _set(XDG_CONFIG_DIRS=ds)
        expected_ds = [self.xdg.XDG_CONFIG_HOME] + ds.split(":")
        expected_ds = os.pathsep.join(expected_ds)
        self.assertEqual(expected_ds, self.xdg.XDG_CONFIG_DIRS)


class TestXDGPackage(unittest.TestCase):
    """
    This test case covers the conditions when a package name is supplied to the
    base :code:`XDG` class, effectively testing package specific directories.
    """

    def setUp(self):
        """Sets up a new object for testing the :class:`XDGPackage` class."""
        self._package_name = "my_awesome_package"
        self.xdg = xdgenvpy.xdgenv.XDGPackage(self._package_name)

    def test_init(self):
        """Validates the initialization of the :class:`XDGPackage: class."""
        package_name = "abc123"
        xdg = xdgenvpy.xdgenv.XDGPackage(package_name)
        # pylint: disable=protected-access
        self.assertEqual(package_name, xdg._package_name)

    def test_init_with_empty_package(self):
        """Tests the condition when the package name is empty."""
        pkg_names = tuple([None, "", "      "])
        for pkg_name in pkg_names:
            with self.assertRaisesRegex(ValueError, "Package name must be specified."):
                self.xdg = xdgenvpy.xdgenv.XDGPedanticPackage(pkg_name)

    def test_data_home(self):
        """Tests the :code:`XDG_DATA_HOME` value."""
        d = "/home/myuser/.local/share"
        _set(XDG_DATA_HOME=d)
        self.assertEqual(
            pathlib.Path(d).joinpath(self._package_name), self.xdg.XDG_DATA_HOME
        )

    def test_config_home(self):
        """Tests the :code:`XDG_CONFIG_HOME` value."""
        d = "/home/myuser/.config"
        _set(XDG_CONFIG_HOME=d)
        self.assertEqual(
            pathlib.Path(d).joinpath(self._package_name), self.xdg.XDG_CONFIG_HOME
        )

    def test_cache_home(self):
        """Tests the :code:`XDG_CACHE_HOME` value."""
        d = "/home/myuser/.cache"
        _set(XDG_CACHE_HOME=d)
        self.assertEqual(
            pathlib.Path(d).joinpath(self._package_name), self.xdg.XDG_CACHE_HOME
        )

    def test_runtime_dir(self):
        """Tests the :code:`XDG_RUNTIME_DIR` value."""
        d = "/run/user/1000"
        _set(XDG_RUNTIME_DIR=d)
        self.assertEqual(
            pathlib.Path(d).joinpath(self._package_name), self.xdg.XDG_RUNTIME_DIR
        )


class TestXDGPedanticPackage(unittest.TestCase):
    """
    Essentially these tests exercise the same functionality as the
    :class:`TestXDGPackage` set of tests, but ensure the package directories
    exist for the user.
    """

    def setUp(self):
        """
        Sets up a new object for testing the :class:`XDGPedanticPackage` class.
        """
        self._tmp_dir = tempfile.mkdtemp()
        self._package_name = "my_awesome_package"
        self.xdg = xdgenvpy.xdgenv.XDGPedanticPackage(self._package_name)

    def tearDown(self):
        """Cleans up the file system from the unit tests."""
        shutil.rmtree(self._tmp_dir)

    def test_init(self):
        """
        Validates the initialization of the :class:`XDGPedanticPackage: class.
        """
        package_name = "abc123"
        xdg = xdgenvpy.xdgenv.XDGPedanticPackage(package_name)
        # pylint: disable=protected-access
        self.assertEqual(package_name, xdg._package_name)

    def test_init_with_empty_package(self):
        """Tests the condition when the package name is empty."""
        pkg_names = tuple([None, "", "      "])
        for pkg_name in pkg_names:
            with self.assertRaisesRegex(ValueError, "Package name must be specified."):
                self.xdg = xdgenvpy.xdgenv.XDGPedanticPackage(pkg_name)

    def test__safe_path(self):
        """
        Validates non-existent directories will be created when referenced.
        """
        d = pathlib.Path(self._tmp_dir).joinpath(".local", "share")
        _set(XDG_DATA_HOME=d)
        self.assertFalse(pathlib.Path(d).joinpath(self._package_name).exists())
        self.assertIsNotNone(self.xdg.XDG_DATA_HOME)
        self.assertTrue(pathlib.Path(d).joinpath(self._package_name).exists())

    def test_data_home(self):
        """
        Validates the directory referenced by :code:`XDG_DATA_HOME` is created
        when accessed.
        """
        d = pathlib.Path(self._tmp_dir).joinpath(".local", "share")
        _set(XDG_DATA_HOME=d)
        self.assertTrue(pathlib.Path(self.xdg.XDG_DATA_HOME).exists())

    def test_config_home(self):
        """
        Validates the directory referenced by :code:`XDG_CONFIG_HOME` is created
        when accessed.
        """
        d = pathlib.Path(self._tmp_dir).joinpath(".config")
        _set(XDG_CONFIG_HOME=d)
        self.assertTrue(pathlib.Path(self.xdg.XDG_CONFIG_HOME).exists())

    def test_cache_home(self):
        """
        Validates the directory referenced by :code:`XDG_CACHE_HOME` is created
        when accessed.
        """
        d = pathlib.Path(self._tmp_dir).joinpath(".cache")
        _set(XDG_CACHE_HOME=d)
        self.assertTrue(pathlib.Path(self.xdg.XDG_CACHE_HOME).exists())

    def test_runtime_dir(self):
        """
        Validates the directory referenced by :code:`XDG_RUNTIME_DIR` is created
        when accessed.
        """
        d = pathlib.Path(self._tmp_dir).joinpath("run/user/10101")
        _set(XDG_RUNTIME_DIR=d)
        self.assertTrue(pathlib.Path(self.xdg.XDG_RUNTIME_DIR).exists())
