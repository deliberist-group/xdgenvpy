# CHANGELOG

## Version 3

### 3.?.? - not yet released

- Created `clean.sh` helper script.
- Minor GitLab CI job changes.

### 3.0.0 - 2024 September 15

- Release script updates.
- Changing tag format to:  release/{new_version}
- Fixing CI pipeline configs.
- Removed usage of `pkg_resources`.
- Refactored how imports are done.
- Virtual environment improvements.

## Version 2

### 2.4.0 - 2024 July 20

- Added support for FreeBSD systems.
- Reformatted code to pass new linters.
- Added scripts to help with testing and releasing.
- Documentation updates.
- Bumpversion config fixes from reformatted code.

### 2.3.5 - 2021 August 29

- Updated logo.
- Added shellcheck and manpage lint jobs to the GitLab CI pipeline.
- Superfluous changes to code comments.
- Mitigated CVE-2018-20225 by adding explicit version numbers to dependencies
  defined in the `requirements*.txt` files.

### 2.3.4 - 2020 December 22

- Fixed the `setup.py twine_check` so it creates the manpages.
- Made portions of the code more version 3 pythonic.
- Random fixes and improvements to CI jobs.
- Added logo image files.

### 2.3.3 - 2020 June 15

- Resolved vulnerabilities detected by GitLab CI Security & Compliance checks.
- Added a tab completion script for Bash.

### 2.3.2 - 2020 June 13

- Fixed `setup.py` so it cleans all compressed man-page files.
- Fixed AppVeyor builds for MacOS.
- Converted license file to markdown.
- Added linter for Markdown.
- Added linter for Python.
- Renamed man page `xdg-env.3` to `xdgenvpy.3`.

### 2.3.1 - 2020 June 11

- Added man pages - section 1 for CLI options, and section 2 for Python API.
- Converted to the MIT license.

### 2.3.0 - 2020 March 25

- Fixed Gitlab CI tests after some CLI options were removed in virtualenv.
- Added badges to README.md.
- Added dates of version release in CHANGELOG.md.
- Updated `setup.py` to include commands `twine_check` and `publish`.
- Updated GitLab CI config with custom unit test job name.
- Fixed Windows tests with AppVeyor, they incorrectly passed.
- Fixed Windows tests with AppVeyor, they incorrectly failed -- yeah ¯\_(ツ)_/¯
- Migrated unit tests to run with PyTest for Gitlab CI and AppVeyor.

### 2.2.2 - 2020 February 17

- Multiple changes to the Gitlab CI so the Security & Compliance jobs work.
- Moved to the "python:latest" Docker image, rather than Ubuntu.
- Refactored the default values to help minimize platform specific logic.
- Added the `-a`/`--all` CLI option to print all XDG variables, which can be
    used quite nicely with the `-d`/`--defaults-only` option.

### 2.2.1 - 2020 January 4

- Moved all dependencies into `requirements*.txt` files.
- Transferred project to deliberist.

### 2.2.0 - 2019 December 14

- Added AppVeyor CI support for MacOS and Windows builds.
- Fixed bugs on MacOS.

### 2.1.1 - 2019 November 19

- Removing the bin script in favor of the setuptools "entry_points".
- Making the main script print the variable names along with the values.
- Made all paths print as POSIX paths, even on Windows.

### 2.1.0 - 2019 November 18

- Added a "-d" option to the main script that only prints default values,  thus
    ignoring any set environment variables.

### 2.0.0 - 2019 November 16

- Added Windows support, which deemed worthy of a major version release.  Though
    the public API has not changed.
- Updated to favor `pathlib` rather than `os.path`.
- Added Development Status and Natural Language project classifiers.
- Added this initial CHANGELOG.md file.
- Updated CONTRIBUTING.md to show how to use bumpversion.
- Removed caching with GitLab CI runners.

## Version 1

### 1.0.3 - 2019 July 9

- Updated docs detailing how to contribute to the project.
- Updated the README docs.

### 1.0.2 - 2019 July 9

- Updated docs detailing how to contribute to the project.
- Fixed bug that only exhibited itself when an invalid XDG variable was
    referenced.

### 1.0.1 - 2019 July 9

- More project renames.
- More documentation updates.

### 1.0.0 - 2019 June 19

- Renamed the bin script to xdgenvpy.
- Updated docs detailing how to contribute to the project.

## Version 0

### 0.1.2 - 2019 May 26

- Renamed the bin script to `xdgpy`.

### 0.1.1 - 2019 May 26

- Updated the README docs.

### 0.1.0 - 2019 May 26

- Updated docs detailing how to contribute to the project.

### 0.0.0

- Initial development without much functionality.
